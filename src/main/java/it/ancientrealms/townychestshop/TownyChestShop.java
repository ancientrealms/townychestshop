package it.ancientrealms.townychestshop;

import org.bukkit.plugin.java.JavaPlugin;

import it.ancientrealms.townychestshop.listener.TownyChestShopListener;

public final class TownyChestShop extends JavaPlugin
{
    private static TownyChestShop instance;

    @Override
    public void onEnable()
    {
        instance = this;

        this.getServer().getPluginManager().registerEvents(new TownyChestShopListener(), this);
    }

    @Override
    public void onDisable()
    {
    }

    public static TownyChestShop getInstance()
    {
        return instance;
    }
}
