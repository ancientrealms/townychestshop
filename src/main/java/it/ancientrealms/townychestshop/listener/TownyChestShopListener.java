package it.ancientrealms.townychestshop.listener;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.Acrobot.ChestShop.Events.PreShopCreationEvent;
import com.Acrobot.ChestShop.Events.PreShopCreationEvent.CreationOutcome;
import com.palmergames.bukkit.towny.TownyAPI;
import com.palmergames.bukkit.towny.object.TownBlockType;

public final class TownyChestShopListener implements Listener
{
    private final TownyAPI townyApi = TownyAPI.getInstance();

    @EventHandler
    public void preShopCreation(PreShopCreationEvent event)
    {
        if (event.getPlayer().hasPermission("townychestshop.bypass"))
        {
            return;
        }

        final Location location = event.getSign().getLocation();
        final boolean flag = this.townyApi.isWilderness(location)
                || Set.of(TownBlockType.EMBASSY, TownBlockType.COMMERCIAL)
                        .stream()
                        .noneMatch(i -> this.townyApi.getTownBlock(location).getType().equals(i));

        if (flag)
        {
            event.setOutcome(CreationOutcome.NO_PERMISSION_FOR_TERRAIN);
            return;
        }
    }
}
